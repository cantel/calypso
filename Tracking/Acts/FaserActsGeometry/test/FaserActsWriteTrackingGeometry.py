#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from FaserActsGeometry.FaserActsWriteTrackingGeometryConfig import FaserActsWriteTrackingGeometryCfg

# Set up logging and new style config
log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
ConfigFlags.Input.Files = ["myevt4.HITS.pool.root"]
#ConfigFlags.Output.RDOFileName = "myRDO_sp.pool.root"
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"             # Always needed; must match FaserVersion
ConfigFlags.GeoModel.FaserVersion = "FASERNU-03"               # Always needed
#ConfigFlags.GeoModel.FaserVersion = "FASERNU-03"               # Always needed
# Workaround for bug/missing flag; unimportant otherwise 
ConfigFlags.addFlag("Input.InitialTimeStamp", 0)
# Workaround to avoid problematic ISF code
ConfigFlags.GeoModel.Layout = "Development"
ConfigFlags.Detector.GeometryFaserSCT = True
#ConfigFlags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01" # Always needed to fool autoconfig; value ignored
ConfigFlags.GeoModel.Align.Dynamic = False
#ConfigFlags.Concurrency.NumThreads = 1
#ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

# Core components
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolReadCfg(ConfigFlags))
#acc.merge(PoolWriteCfg(ConfigFlags))
from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
acc.merge(FaserGeometryCfg(ConfigFlags))

# Inner Detector
acc.merge(FaserActsWriteTrackingGeometryCfg(ConfigFlags))

# Output Stream customization
#oStream = acc.getEventAlgo("OutputStreamRDO")
#oStream.ItemList += ["EventInfo#*",
#                     "McEventCollection#TruthEvent",
#                     "McEventCollection#BeamTruthEvent"
#                    ]
                    
# Timing
#acc.merge(MergeRecoTimingObjCfg(ConfigFlags))

# Dump config
logging.getLogger('forcomps').setLevel(VERBOSE)
acc.foreach_component("*").OutputLevel = VERBOSE
acc.foreach_component("*ClassID*").OutputLevel = INFO
#acc.getCondAlgo("FaserActsAlignmentCondAlg").OutputLevel = VERBOSE
#acc.getCondAlgo("FaserNominalAlignmentCondAlg").OutputLevel = VERBOSE
acc.getService("StoreGateSvc").Dump = True
acc.getService("ConditionStore").Dump = True
acc.printConfig(withDetails=True)
ConfigFlags.dump()
# Execute and finish
sc = acc.run(maxEvents=1)
# Success should be 0
sys.exit(not sc.isSuccess())
